#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.generic import TemplateView

from .functions import *

# Error Pages
def miError404(request):
    return render_to_response('404.html', status=404)

def miError403(request):
    return render_to_response('403.html', status=403)

def miError500(request):
    return render_to_response('500.html', status=500)

# Ajax
class AjaxLoadMoreArticles(TemplateView):

    def get(self, request, *args, **kwargs):
        lista = []
        parametro = False

        try:
            localizacion = int(request.GET.get('localizacion', '0'))
            lsup = int(request.GET.get('lsup', '0'))
            categoria_id = int(request.GET.get('categoria', '0'))
        except:
            raise Exception

        page = int(request.GET.get('page', '0'))

        if page == 1:  # Home
            home = Home.objects.live().get(slug="el-noticiero")
            lista, on_screen, parametro = query_location_articles(home, localizacion, lsup)

        elif page == 2:  # NewsCategory
            if categoria_id != 0:
                try:
                    category = Page.objects.get(id=categoria_id)
                    if category.slug == "terremoto-ecuador":
                        lista, parametro = query_category_event(lsup)
                    else:
                        lista, parametro = query_category_location_articles(category, localizacion, lsup)

                except NewsCategory.DoesNotExist:
                    raise Http404

        elif page == 3:  # Videoteca
            lista, parametro = query_video_archive(localizacion, lsup)


        return render_to_response('core/ajax/load-articles.html',
                                  {'articles': lista, 'parametro': parametro, "page": page},
                                  context_instance=RequestContext(request))

# Subscribe
class SubscribeViews(TemplateView):
    template_name = 'core/subscribe_temp.html'

# Query for Iframes
def iframe_nacional(request):
    '''
    Query for get Pages Nacional
    :param request:
    :return:
    '''
    try:
        # Entretenimiento/Farandula
        entretenimiento = NewsCategory.objects.get(slug='entretenimiento')
        # Cuidado si cambian el slug de deportes
        deportes = NewsCategory.objects.get(slug='deportes')
    except:
        raise ValidationError('Error en el servidor al encontrar la categoía')
    lista_ini = \
        Page.objects.live().filter(depth=5).exclude(newsarticles__categoria=entretenimiento).exclude(
            newsarticles__categoria=deportes).exclude(
            newsarticlesonscreen__categoria=entretenimiento).exclude(newsarticlesonscreen__categoria=deportes
        ).live().filter(Q(newsarticles__localizacion=1) | Q(newsarticlesonscreen__localizacion=1)).type((NewsArticles,
        NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:3]
    response = render_to_response('core/iframes/iframe-article.html', {"articles": lista_ini, "clase": "purple"})
    response['Cache-Control'] = 'max-age=3800'
    return response

def iframe_internacional(request):
    '''
    Query for get Pages Internacional
    :param request:
    :return:
    '''
    try:
        # Entretenimiento/Farandula
        entretenimiento = NewsCategory.objects.get(slug='entretenimiento')
        # Cuidado si cambian el slug de deportes
        deportes = NewsCategory.objects.get(slug='deportes')
    except:
        raise ValidationError('Error en el servidor al encontrar la categoía')

    lista_ini = \
        Page.objects.live().filter(depth=5).exclude(newsarticles__categoria=entretenimiento).exclude(
            newsarticles__categoria=deportes).exclude(
            newsarticlesonscreen__categoria=entretenimiento).exclude(newsarticlesonscreen__categoria=deportes
        ).live().filter(Q(newsarticles__localizacion=2) | Q(newsarticlesonscreen__localizacion=2)).type((NewsArticles,
        NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:3]
    response = render_to_response('core/iframes/iframe-article.html', {"articles": lista_ini, "clase": "purple"})
    response['Cache-Control'] = 'max-age=3800'
    return response

def iframe_entretenimiento(request):
    '''
    Query for get Pages Entretenimiento Nacional
    :param request:
    :return:
    '''
    # Entretenimiento/Farandula
    category = NewsCategory.objects.get(slug='entretenimiento')
    lista_ini = category.get_children().live().filter(Q(newsarticles__localizacion=1) |
                            Q(newsarticlesonscreen__localizacion=1)).type((NewsArticles,
                            NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:3]
    response = render_to_response('core/iframes/iframe-article.html', {"articles": lista_ini, 'flag': 1,
                                                                       "clase": "magenta"})
    response['Cache-Control'] = 'max-age=3800'
    return response

