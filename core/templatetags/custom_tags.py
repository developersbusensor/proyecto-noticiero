#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib
from bs4 import BeautifulSoup

from django.core.urlresolvers import reverse
from django.utils.timesince import timesince
from django.utils.dateformat import format
from django.core.exceptions import ValidationError

from wagtail.wagtailcore.models import Site
from wagtail.wagtailimages.templatetags.wagtailimages_tags import *

from core.models import *
from core.functions import query_location_articles, query_category_location_articles, \
    query_articles_realted, query_latest_articles, query_video_archive, query_video_archive_reporter, \
    escape, query_category_event

__author__ = 'developerbusensor'
register = template.Library()

# CORE
# NOTA CABE RECORDAR QUE SI SE CAMBIAN LOS SLUGS DE CATEGORía habrá problemas de redireccionamiento en mobile y demás
@register.simple_tag(takes_context=False)
def pageurl_slug(slug):
    url = "#"
    try:
        page = Page.objects.get(slug=slug)
        return page.url
    except:
        return url

@register.simple_tag(takes_context=True)
def pageurl_custom(context, page):
    """
    Outputs a page's URL as relative (/foo/bar/) if it's within the same site as the
    current page, or absolute (http://example.com/foo/bar/) if not.
    """
    try:
        return page.relative_url(context['request'].site)
    except:
        return '#'

@register.simple_tag(takes_context=False)
def pageurl_encode(page):
    """
    Outputs url encode for share, be careful version python 2.7 and python 3.x
    """
    url_full = ""
    if not page:
        return ' '
    try:
        for (id, root_path, root_url) in Site.get_site_root_paths():
            if page.url_path.startswith(root_path):
                url_full = root_url + reverse('wagtail_serve', args=(page.url_path[len(root_path):],))
        url = {'url_path': url_full}
        result = urllib.urlencode(url).split("=")[1]  # (version 2.7)
        # result = urllib.parse.urlencode(url).split("=")[1]  # (version 3.x)
    except:
        result = '#'  # urllib.parse.urlencode(url).split("=")[1]  # (version 3.x)
    return result

@register.simple_tag(takes_context=False)
def analitycs_custom(social, value):
    '''
    Filter that get value = utm_medium, recive var social that is a number about this case and var utm.
    :param social:
    :param value:
    :return:
    '''

    if value:
        FB = '%3Futm_source%3DFACEBOOK%26utm_campaign%3DSHARE%26utm_medium%3D'
        TW = '%3Futm_source%3DTWITTER%26utm_campaign%3DSHARE%26utm_medium%3D'
        WAPP = '%3Futm_source%3DWHATSAPP%26utm_campaign%3DSHARE%26utm_medium%3D'
        MAIL = '%3Futm_source%3DMAIL%26utm_campaign%3DSHARE%26utm_medium%3D'
        parametro = value.replace(" ", "%20").replace("-", "%2D").replace("|",
                                                                          "%7C").replace("=", "%3D").replace("?", "%3F")

        if social == 1:  # Fb
            return FB+parametro
        if social == 2:  # Tw
            return TW+parametro
        if social == 3:  # Wapp
            return WAPP+parametro
        if social == 4:  # Mail
            return MAIL+parametro
    else:
        return ''

@register.simple_tag(takes_context=False)
def event_custom_score(event, value):
    '''
    Filter that get value = utm_medium, recive var social that is a number about this case and var utm.
    :param social:
    :param value:
    :return:
    '''
    now = datetime.datetime.now()
    if now.date() > event.fecha:
        return str(event.equipo1.score)+' - '+str(event.equipo2.score)
    else:
        if value > now.time():
            return value.strftime("%H:%M")
        else:
            return str(event.equipo1.score)+' - '+str(event.equipo2.score)

# FILTERS
# Truncate chars but leaving last word complete
@register.filter("smart_truncate_chars")
def smart_truncate_chars(value, max_length):
    if len(value) > max_length:
        # Limits the number of characters in value tp max_length (blunt cut)
        truncd_val = value[:max_length]
        # Check if the next upcoming character after the limit is not a space,
        # in which case it might be a word continuing
        if value[max_length] != " ":
        # rfind will return the last index where matching the searched character,
        # in this case we are looking for the last space
        # Then we only return the number of character up to that last space
            truncd_val = truncd_val[:truncd_val.rfind(" ")]
        return truncd_val + "..."
    return value

@register.filter(name='get_code_format_video')
def get_code_format_video(value):
    '''
    Function that return code url
    :param url:
    :return:
    '''
    try:
        # python 3.x
        from urllib.parse import urlparse
        if not value:
            return ''
        result = urlparse(value).path.split('/')[2].split('_')[0]
    except:
        # python 2.7
        from urlparse import urlparse
        #from urllib.parse import urlparse
        if not value:
            return ''
        try:
            result = urlparse(value).path.split('/')[2].split('_')[0]
        except:
            result = urlparse(value).path.split('/')[1]
    return result

@register.filter(name="time_filter")
def time_filter(value):
    '''
    Formats a date as the time since that date (i.e. "4 days, 6 hours").
    :param value:
    :return:
    '''
    if not value:
        return ''
    return 'Hace '+timesince(value)

@register.filter(name="time_filter_custom")
def time_filter_custom(value):
    '''
    Formats a date as the time since or dateformat ej: Noviembre 4, 2015 or Hace 5 minutos
    :param value:
    :return:
    '''
    now = datetime.datetime.now()
    arg = 'F d, Y'
    if not value:
        return ''
    if value.date() < now.date():
        return format(value, arg)
    else:
        return 'Hace '+timesince(value)

@register.filter(name='encode_text')
def encode_text(str):
    '''
    Outputs url encode for share, be careful version python 2.7 and python 3.x
    :param str:
    :return:
    '''
    cad = slugify(str)
    result = cad.replace(" ", "%20").replace("-", "%2D").replace("|", "%7C").replace("=", "%3D").replace("?", "%3F")
    return result
    '''
    str.replace(" ", "%20").replace("-", "%2D").replace("ñ", "%C3%B1").\
    replace("Ñ", "%C3%91").replace("Á", "%C3%81").replace("á", "%C3%A1").replace("É", "%C3%89").\
    replace("é", "%C3%A9").replace("í", "%C3%AD").replace("Í", "%C3%8D").replace("ó", "%C3%B3").\
    replace("Ó", "%C3%93").replace("ú", "%C3%BA").replace("Ú", "%C3%9A")
    '''

# truncate chars but leaving last word complete
@register.filter(name='smarttruncatechars')
def smart_truncate_chars(value, max_length):
    if len(value) > max_length:
        # limits the number of characters in value to max_length (blunt cut)
        truncd_val = value[:max_length]
        # check if the next upcoming character after the limit is not a space,
        # in which case it might be a word continuing
        if value[max_length] != ' ':
            # rfind will return the last index where matching the searched character,
            # in this case we are looking for the last space
            # then we only return the number of character up to that last space
            truncd_val = truncd_val[:truncd_val.rfind(' ')]
        return truncd_val + '...'

    return value

@register.filter(name='custom_iframes')
def custom_iframes(value):
    '''
    Filter that add data-src for all types streamfields how html-embed and video
    :param value:
    :return:
    '''
    soup = BeautifulSoup(value, 'html.parser')
    if soup.iframe:
        tag_iframe = soup.iframe
        resource = tag_iframe['src']
        del soup.iframe['src']
        soup.iframe['data-src'] = resource
        return escape(str(soup.iframe))
    else:
        return value

# HOME
'''
    2.- Noticias Destacadas (Fusionar en le panel admin NewsArticle - NewsArticleOnScreen)
'''
@register.inclusion_tag('core/tags/home/articles-home.html', takes_context=True)
def articles_home(context, obj):
    url_path = context['request'].build_absolute_uri()

    if '?' in url_path:
        parameter = url_path.split("?")[1].split("=")[0]

        if parameter != "localizacion":
            raise Http404

        else:

            parameter_value = context['request'].GET.get('localizacion', '0')

            if parameter_value.isnumeric():
                localizacion = int(parameter_value)
                lsup = int(context['request'].GET.get('lsup', '0'))
            else:
                raise ValidationError('Error en el servidor el parametro ingresado es incorrecto')
    else:
        localizacion = 0
        lsup = 0

    lista, on_screen, parametro = query_location_articles(obj, localizacion, lsup)

    return {"articles": lista, "lsup": lsup, "parametro": parametro,
            "panel": obj.uso_noticias_descatadas, "home": obj,
            "on_screen": on_screen, 'request': context['request']}

# VIDEO ARCHIVE
@register.inclusion_tag('core/tags/video-archive/main-content.html', takes_context=True)
def video_archive_main_content(context, obj):
    url_path = context['request'].build_absolute_uri()
    if '?' in url_path:
        validator_url = url_path.split("?")
        if validator_url[1].split("=")[0] != "localizacion":
            raise Http404
    try:
        localizacion = int(context['request'].GET.get('localizacion', '0'))
        lsup = int(context['request'].GET.get('lsup', '0'))
    except:
        # Error 500
        raise Exception('Error en el servidor el parametro ingresado es incorrecto')

    articles_tv, parametro = query_video_archive(localizacion, lsup)

    return {'request': context['request'], "parametro": parametro,
            'self': obj, 'articles': articles_tv}

# VIDEO ARCHIVE REPORTER
@register.inclusion_tag('core/tags/video-archive/main-content-reporter.html', takes_context=True)
def video_archive_main_content_reporter(context, obj):
    try:
        lsup = int(context['request'].GET.get('lsup', '0'))
    except:
        # Error 500
        raise Exception('Error en el servidor el parametro ingresado es incorrecto')

    articles_tv, parametro = query_video_archive_reporter(obj, lsup)

    return {'request': context['request'], "parametro": parametro,
            'self': obj, 'articles': articles_tv}

# CATEGORY
@register.inclusion_tag('core/tags/news-category/articles-category.html', takes_context=True)
def articles_category(context, category):
    url_path = context['request'].build_absolute_uri()
    if '?' in url_path:
        validator_url = url_path.split("?")
        if validator_url[1].split("=")[0] != "localizacion":
            raise Http404
    try:
        localizacion = int(context['request'].GET.get('localizacion', '0'))
        lsup = int(context['request'].GET.get('lsup', '0'))
    except:
        # Error 500
        raise Exception('Error en el servidor el parametro ingresado es incorrecto')

    if category.slug == 'terremoto-ecuador':
        lista, parametro = query_category_event(lsup)
    else:
        lista, parametro = query_category_location_articles(category, localizacion, lsup)

    return {'articles': lista, 'request': context['request'], "parametro": parametro}

# ARTICLES
@register.inclusion_tag('core/tags/news-articles/related-news.html', takes_context=True)
def related_articles(context, article):

    if article.categoria is not None:
        categoria = Page.objects.live().get(id=article.categoria.id)
    else:
        categoria = Page.objects.live().get(id=article.get_ancestors().type(NewsCategory).last().id)

    articles, parametro = query_articles_realted(article, categoria)

    return {'articles': articles, "categoria": categoria, 'parametro': parametro,
            'article': article, 'request': context['request']}

@register.inclusion_tag('core/tags/news-articles/latest-news.html', takes_context=True)
def latest_articles(context, article):
    if article.categoria is not None:
        categoria = Page.objects.live().get(id=article.categoria.id)
    else:
        categoria = Page.objects.live().get(id=article.get_ancestors().type(NewsCategory).last().id)

    articles, parametro = query_latest_articles(article, categoria)

    return {'articles': articles, 'parametro': parametro, 'request': context['request']}

@register.inclusion_tag('core/tags/home/events.html', takes_context=True)
def events(context):
    return {
        'events': Event.objects.all(),
        'request': context['request'],
    }