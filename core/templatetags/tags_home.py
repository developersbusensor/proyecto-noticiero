from django import template

from core.models import *

__author__ = 'developerbusensor'

register = template.Library()

# Enlace a la Pagina Ruta
@register.assignment_tag(takes_context=True)
def get_site_root(context):
    return context['request'].site.root_page

@register.inclusion_tag('core/includes/base/template_snippets/categories_menu.html', takes_context=True)
def top_menu(context):

    categories = NewsCategory.objects.live().in_menu().order_by('prioridad')

    return {'categories': categories,
            # required by the pageurl tag that we want to use within this template
            'request': context['request']}

@register.inclusion_tag('core/includes/base/template_snippets/menu-mobile.html', takes_context=True)
def menu_mobile(context, page):
    categories = NewsCategory.objects.live().in_menu().order_by('prioridad')

    return {'categories': categories, 'page': page, 'request': context['request']}


