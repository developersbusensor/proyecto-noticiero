from django.utils.safestring import mark_safe

from django.utils.html import format_html, format_html_join
from django.conf import settings
from wagtail.wagtailcore import hooks

def editor_js():
    js_files = [
        'js/vendor/rangy-core1.3.js',
        'js/vendor/jquery.htmlClean.js',
        'js/vendor/rangy-selectionsaverestore.js',

        'js/core/hallo-custom.js',
        'js/core/core-admin.js',
    ]
    js_includes = format_html_join('\n', '<script src="{0}{1}"></script>',
        ((settings.STATIC_URL, filename) for filename in js_files)
    )
    return js_includes
hooks.register('insert_editor_js', editor_js)


def editor_css():
    '''
        For that file .css funtion rigth.
        We must modify js (hallo-wagtailembeds.js, hallo-wagtailimage.js, hallo-wagtaildoclink.js)
        button = $('<span class="'+this.widgetName +'"></span>');
    '''
    return format_html('<link rel="stylesheet" href="'+ settings.STATIC_URL +
                       'css/core/hallo-plugin.css">')

hooks.register('insert_editor_css', editor_css)