#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from core.models import *

# Register your models here.
from django.shortcuts import get_object_or_404
from wagtail.wagtailcore.models import Page
import datetime

admin.site.register(Tipo_Redactor)
admin.site.register(Redactor)
admin.site.register(Localizacion)

class EquipoAdmin(admin.ModelAdmin):
    ordering = ('descripcion',)
    fields = ('descripcion', 'siglas', 'photo', 'score')
    list_display = ('descripcion',  'score')
    list_editable = ('score',)
    search_fields = ['descripcion']

admin.site.register(Equipo, EquipoAdmin)

# Page
class NewsArticleAdmin(admin.ModelAdmin):
    ordering = ('-first_published_at',)
    fields = ('title', 'categoria')
    list_display = ('title', 'categoria', 'date_publication', 'evento')
    list_editable = ('evento',)

    search_fields = ['title', 'categoria__title']
    list_filter = ('categoria', 'live', 'localizacion', 'first_published_at', 'owner')

    def upper_owner(self, obj):
        return ("%s %s" % (obj.owner.first_name, obj.owner.last_name)).upper()

    def date_publication(self, obj):
        return obj.first_published_at

    upper_owner.short_description = u'Publicado Por'
    date_publication.short_description = u'Fecha de Publicación'

# Page
class NewsArticleOnScreenAdmin(admin.ModelAdmin):
    ordering = ('-first_published_at',)
    fields = ('title', 'categoria')
    list_display = ('title', 'categoria', 'date_publication', 'evento')
    list_editable = ('evento',)

    search_fields = ['title', 'categoria__title']
    list_filter = ('categoria', 'live', 'localizacion', 'first_published_at', 'owner')

    def upper_owner(self, obj):
        return ("%s %s" % (obj.owner.first_name, obj.owner.last_name)).upper()

    def date_publication(self, obj):
        return obj.first_published_at

    upper_owner.short_description = u'Publicado Por'
    date_publication.short_description = u'Fecha de Publicación'

admin.site.register(NewsArticles, NewsArticleAdmin)
admin.site.register(NewsArticlesOnScreen, NewsArticleOnScreenAdmin)

class VideoArchiveAdmin(admin.ModelAdmin):
    ordering = ('-first_published_at',)
    fields = ('title', )
    list_display = ('title', )

    def save_model(self, request, obj, form, change):
        # Obtener el padre page que debe ser el page (noticias)
        parent_page = get_object_or_404(Page,  slug='el-noticiero').specific
        # Save page
        parent_page.add_child(instance=obj)
        # Save revision
        revision = obj.save_revision(user=request.user, submitted_for_moderation=True, )
        # Publish
        revision.publish()
        obj.slug = slugify(obj.title)
        obj.set_url_path(parent_page)
        obj.first_published_at = datetime.datetime.now()
        obj.live = True
        obj.depth = 3
        obj.save()

admin.site.register(VideoArchive, VideoArchiveAdmin)

class NewsCategoryAdmin(admin.ModelAdmin):

    ordering = ('-first_published_at',)
    fields = ('title', 'prioridad',)

    '''
    def save_model(self, request, obj, form, change):
        # Obtener el padre page que debe ser el page (noticias)
        parent_page = get_object_or_404(Page,  slug='noticias').specific
        # Save page
        parent_page.add_child(instance=obj)
        # Save revision
        revision = obj.save_revision(user=request.user, submitted_for_moderation=True, )
        # Publish
        revision.publish()
        obj.slug = slugify(obj.title)
        obj.set_url_path(parent_page)
        obj.first_published_at = datetime.datetime.now()
        obj.live = True
        obj.depth = 4
        obj.save()
    '''
admin.site.register(NewsCategory, NewsCategoryAdmin)
