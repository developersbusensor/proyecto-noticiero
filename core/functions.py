import json

import requests
from django.core.exceptions import ValidationError
from django.http import Http404
from django.utils.safestring import mark_safe
from django.db.models import Q

from .models import *

__author__ = 'user3'

def get_first(iterable, default=None):
    if iterable:
        for item in iterable:
            return item
    return default

def escape(html):
    """Returns the given HTML with ampersands, quotes and carets encoded."""
    # print "escape: ", mark_safe(unicode(html).replace('&lt;', '<').replace('&gt;', '>'))
    return mark_safe(unicode(html).replace('&lt;', '<').replace('&gt;', '>'))

def get_panel_outstandingnews(noticias):
    '''
    Function that
    :param noticias:
    :return:
    '''
    lista = []
    if noticias.uso_noticias_descatadas:
        # Panel de noticias destacadas
        for i in noticias.noticias_destacadas.all():
            if i.noticia is not None:
                if NewsArticles.objects.live().filter(id=i.noticia.id).exists():
                    article = NewsArticles.objects.live().get(id=i.noticia.id)
                else:
                    article = NewsArticlesOnScreen.objects.live().get(id=i.noticia.id)
                lista.append(article)
            else:
                lista.append(i.noticia)
    return lista

# Querys Articles
def query_location_articles(obj, location, lsup):
    '''
    Function that executes query for NewsArticle / NewsArticlesOnScreen
    :param location:
    :return:
    '''
    parametro = False

    if location == 0:
        entretenimiento = NewsCategory.objects.get(slug='entretenimiento')
        on_screen = NewsArticlesOnScreen.objects.live().order_by("-first_published_at")[:3]
        lista_ini = Page.objects.live().filter(depth=5).type((NewsArticles, NewsArticlesOnScreen)
                                                             ).specific().exclude(
            newsarticlesonscreen__localizacion=1, newsarticlesonscreen__categoria=entretenimiento
        ).order_by("-first_published_at")[:60]

    elif location == 1:  # Nacional
        on_screen = NewsArticlesOnScreen.objects.live().filter(localizacion=location).order_by("-first_published_at")[0:3]
        lista_ini = Page.objects.live().filter(
            depth=5).filter(Q(newsarticles__localizacion=location) |
                            Q(newsarticlesonscreen__localizacion=location)).type((NewsArticles, NewsArticlesOnScreen)
                                                             ).specific().order_by("-first_published_at")[:50]
    elif location == 2:  # Internacional
        on_screen = NewsArticlesOnScreen.objects.live().filter(localizacion=location).order_by("-first_published_at")[0:3]
        lista_ini = Page.objects.live().filter(
            depth=5).filter(Q(newsarticles__localizacion=location) | Q(newsarticlesonscreen__localizacion=location)
                            ).type((NewsArticles, NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:50]
    else:
        raise Http404

    if obj.uso_noticias_descatadas:
        page_news = Home.objects.live().get(slug='el-noticiero')
        panel_destacado = get_panel_outstandingnews(page_news)
        none_list = [None]*7
        if len(panel_destacado) < 7:
            panel = (panel_destacado + none_list)[0:7]
        else:
            panel = panel_destacado

    if lista_ini.count() > 14:
        parametro = True
    if lista_ini.count() <= lsup:
        parametro = False
    if lsup > 0:
        lista_ini = lista_ini[14:lsup]

    return lista_ini, on_screen, parametro

def query_category_event(lsup):
    '''
    Function that exexutes query
    :param category:
    :param lsup:
    :return:
    '''
    parametro = False
    lista_ini = Page.objects.live().type((NewsArticles, NewsArticlesOnScreen)).specific(
    ).filter(Q(newsarticles__evento=True) | Q(newsarticlesonscreen__evento=True)).order_by("-first_published_at")[:100]

    if lista_ini.count() > 11:
        parametro = True
    if lista_ini.count() <= lsup:
        parametro = False
    if lsup > 0:
        lista_ini = lista_ini[11:lsup]

    return lista_ini, parametro

def query_category_location_articles(category, location, lsup):
    '''
    Function that executes query for NewsArticle / NewsArticlesOnScreen
    :param location:
    :return:
    '''
    parametro = False

    if location == 0:
        if category.slug == 'ecuador-elige':
            lista_ini = category.get_children().live().type(
                (NewsArticles, NewsArticlesOnScreen)).specific()  # ecuador - elige
            lpolitica = Page.objects.live().filter(depth=5).type(
                (NewsArticles, NewsArticlesOnScreen)
            ).specific().filter(Q(newsarticles__evento=True) |
                                Q(newsarticlesonscreen__evento=True)).order_by("-first_published_at")[:60]  # selected
            lista_ini = lista_ini | lpolitica

        elif category.slug == 'de-boca-en-boca':  # bruned hard code - 04/10/2017
            entretenimiento = NewsCategory.objects.get(slug='entretenimiento')
            lista_ini = Page.objects.live().filter(depth=5).type(NewsArticlesOnScreen).specific().filter(
                    newsarticlesonscreen__localizacion=1, newsarticlesonscreen__categoria=entretenimiento
                ).order_by("-first_published_at")[:60]
        else:
            lista_ini = category.get_children().live().type(
                (NewsArticles, NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:50]

    elif location == 1:  # Nacional
        lista_ini = category.get_children().live().filter(Q(newsarticles__localizacion=location) |
                            Q(newsarticlesonscreen__localizacion=location)).type((NewsArticles,
                            NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:50]
    elif location == 2:  # Internacional
        lista_ini = category.get_children().live().filter(Q(newsarticles__localizacion=location) |
                            Q(newsarticlesonscreen__localizacion=location)).type((NewsArticles,
                            NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:50]
    else:
        raise Http404

    if lista_ini.count() > 11:
        parametro = True
    if lista_ini.count() <= lsup:
        parametro = False
    if lsup > 0:
        lista_ini = lista_ini[11:lsup]

    return lista_ini, parametro


def query_video_archive(location, lsup):
    '''
    :param location:
    :param q:
    :return:
    '''
    parametro = False

    if location == 0:
        articles_tv = NewsArticlesOnScreen.objects.live().order_by("-first_published_at")[:50]
    elif location == 1:
        articles_tv = NewsArticlesOnScreen.objects.live().filter(localizacion=location
                                                                 ).order_by("-first_published_at")[:50]
    elif location == 2:
        articles_tv = NewsArticlesOnScreen.objects.live().filter(localizacion=location
                                                                 ).order_by("-first_published_at")[:50]
    else:
        raise Http404

    if len(articles_tv) > 8:
        parametro = True

    if len(articles_tv) <= lsup:
        parametro = False

    if lsup > 0:
        articles_tv = articles_tv[8:lsup]

    return articles_tv, parametro


def query_video_archive_reporter(obj, lsup):
    '''

    :param lsup:
    :return:
    '''
    parametro = False

    articles_tv = NewsArticlesOnScreen.objects.live().filter(reportero=obj).order_by("-first_published_at")[:50]
    if len(articles_tv) > 8:
        parametro = True
    if len(articles_tv) <= lsup:
        parametro = False

    if lsup > 0:
        articles_tv = articles_tv[8:lsup]
    return articles_tv, parametro

# Related Articles
def query_articles_realted(article, categoria):
    '''
    Function
    :param article:
    :param categoria:
    :return:
    '''
    parametro = False
    lista = categoria.get_children().live().exclude(id=article.id).type(
        (NewsArticles, NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:4]

    if lista.count() > 2:
        parametro = True

    return lista, parametro

# Latest Articles
def query_latest_articles(article, categoria):
    '''
    Function
    :param article:
    :param categoria:
    :return:
    '''
    parametro = False
    lista_ini = Page.objects.live().filter(depth=5).exclude(id=article.id).exclude(
        newsarticles__categoria=categoria).exclude(newsarticlesonscreen__categoria=categoria).type(
        (NewsArticles, NewsArticlesOnScreen)).specific().order_by("-first_published_at")[:4]

    if lista_ini.count() > 2:
        parametro = True

    return lista_ini, parametro

def short_url(url):
    '''
    Function
    :param url:
    :return:
    '''
    post_url = 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAPu2uSqOhNb0_sMGqEXp-mKkLc-xZ1OF0'
    params = json.dumps({'longUrl': url})
    response = requests.post(post_url, params, headers={'Content-Type': 'application/json'})
    return response.json()['id']