#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import slugify
from django.shortcuts import render
from django.http import Http404

from modelcluster.fields import ParentalKey
from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, \
    PageChooserPanel, InlinePanel
from wagtail.wagtailadmin.edit_handlers import TabbedInterface, ObjectList
from wagtail.wagtailcore import blocks
from wagtail.wagtailcore.blocks import RawHTMLBlock
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel

from libraries.general_features import get_codeUrl, format_intro, get_share_image, get_share_image_dbeb
from libraries.clases import ArticleAPI

from administracion.formularios.FormAdmin import *

import urllib2
import json

# MODELS
class Tipo_Redactor(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    class Meta:
        verbose_name = "tipo_redactor"
        verbose_name_plural = "tipo_redactores"

    def __str__(self):  # __unicode__ en Python 2
         return '%s' % self.descripcion

class Redactor(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_redactor = models.ForeignKey(Tipo_Redactor)
    nombres = models.TextField()
    twitter = models.CharField(max_length=200)
    # foto
    email = models.EmailField()

    class Meta:
        verbose_name = "redactor"
        verbose_name_plural = "redactores"

    def __str__(self):  # __unicode__ en Python 2
         return '%s' % self.nombres


class Localizacion(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)

    class Meta:
        verbose_name = "Localizacion"
        verbose_name_plural = "Localizacion"

    def __str__(self):  # __unicode__ en Python 2
         return '%s' % self.descripcion

# BLOCKS STREAMFIELDS
class BlockquoteBlock(blocks.StructBlock):
    quote = blocks.TextBlock(label="Frase", required=True)
    author = blocks.TextBlock(label="Autor", required=True)
    photo_author = ImageChooserBlock(label="Foto", required=True)

    class Meta:
        icon = "icon icon-openquote"
        template = "core/includes/news-articles/block-streamfields/blockquote.html"
        label = "Cita"

class SubTitleBlock(blocks.StructBlock):
    subtitle = blocks.CharBlock(label="Subtítulo", classname="full title")

    class Meta:
        icon = "icon icon-title"
        template = "core/includes/news-articles/block-streamfields/subtitle.html"
        label = "Subtítulo"

class BlockStreamFieldContentArticleWeb(blocks.StreamBlock):
    subtitle = SubTitleBlock()
    paragraph = blocks.RichTextBlock(icon="icon icon-pilcrow", label="Párrafo")
    phrase = blocks.TextBlock(icon="icon icon-pick", label="Frase",
                              template="core/includes/news-articles/block-streamfields/phrase.html")
    blockquote = BlockquoteBlock()
    image = ImageChooserBlock(icon="icon icon-image", label="Normal",
                              template="core/includes/news-articles/block-streamfields/image.html")
    panorama = ImageChooserBlock(icon="icon icon-image", label="Panorámica",
                                 template="core/includes/news-articles/block-streamfields/panorama.html")
    '''
    # FORMAT URL
    video = EmbedBlock(icon="icon icon-media", label="Video",
                       template="core/includes/news-articles/block-streamfields/video.html")
    '''
    # FORMAT EMBED
    video2 = RawHTMLBlock(icon="icon icon-media", label="Video",help_text="Insertar el código Embebido del Video",
                             template="core/includes/news-articles/block-streamfields/video.html")
    htmlembed = RawHTMLBlock(icon="icon icon-code", label="Insertar Código",
                             help_text="No utilice este botón para insertar videos",
                             template="core/includes/news-articles/block-streamfields/htmlembed.html")

class BlockStreamFieldContentArticleOnScreen(blocks.StreamBlock):
    paragraph = blocks.RichTextBlock(icon="icon icon-pilcrow", label="Párrafo")
    video = RawHTMLBlock(icon="icon icon-media", label="Video", help_text="Insertar el código Embebido del Video")
    htmlembed = RawHTMLBlock(icon="icon icon-code", label="Insertar Código",
                             help_text="No utilice este botón para insertar videos",
                             template="core/includes/news-articles/block-streamfields/htmlembed.html")

# FIELDS
class ArticleFields(models.Model):
    titulo = models.CharField(max_length=200,
                              help_text="El título de la página como quieres que sea visto por el público",
                              verbose_name=u"Título")
    photo = models.ForeignKey('wagtailimages.Image', null=True, on_delete=models.SET_NULL, related_name='+',
                              verbose_name="Foto de Artículo")
    photo_fb_share = models.ImageField(upload_to='shares/fb', null=True, blank=True)
    photo_tw_share = models.ImageField(upload_to='shares/tw', null=True, blank=True)
    categoria = models.ForeignKey('core.NewsCategory', blank=True, null=True, on_delete=models.SET_NULL,
                                  verbose_name="Categoría")
    redactor = models.ForeignKey(Redactor, blank=True, null=True, on_delete=models.SET_NULL)
    introduction = models.TextField(max_length=200, null=True, blank=True)
    on_screen = models.BooleanField(default=False, blank=True)
    localizacion = models.ForeignKey(Localizacion, null=True, on_delete=models.SET_NULL, verbose_name="Localización")
    evento = models.BooleanField(default=False, blank=True, help_text="Clasifica artículos de los diversos eventos. "
                                                                         "Ej: Terremoto Ecuador")
    fecha_pub = models.DateField(null=True, default=datetime.datetime.now().date())
    hora_pub = models.TimeField(null=True, default=datetime.datetime.now().time())

    class Meta:
        abstract = True


# A couple of abstract classes that contain commonly used fields
class OutstandingNewsFields(models.Model):
    noticia = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    @property
    def link(self):
        if self.link_page:
            return self.link_page.url

    panels = [PageChooserPanel('noticia', ['core.NewsArticles', 'core.NewsArticlesOnScreen']), ]

    class Meta:
        abstract = True

class HomeOutstandingNews(Orderable, OutstandingNewsFields):
    page = ParentalKey('core.Home', related_name='noticias_destacadas')

class Equipo(models.Model):
    descripcion = models.CharField(max_length=255)
    photo = models.ForeignKey('wagtailimages.Image', null=True,
                              on_delete=models.SET_NULL, related_name='+',
                              verbose_name="Foto")
    siglas = models.CharField(max_length=100)
    score = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Equipo"
        verbose_name_plural = "Equipos"

    def __unicode__(self):
        return self.descripcion

@register_snippet
class Event(models.Model):
    descripcion = models.CharField(max_length=255)
    fecha = models.DateField(null=True)
    hora = models.TimeField(null=True)
    equipo1 = models.ForeignKey('Equipo', null=True, related_name='equipo1', blank=True)
    equipo2 = models.ForeignKey('Equipo', null=True, related_name='equipo2', blank=True)
    photo = models.ForeignKey('wagtailimages.Image', null=True, blank=True,
                              on_delete=models.SET_NULL, related_name='+',
                              verbose_name="Foto")

    estado = models.IntegerField(default=1)

    panels = [
        FieldPanel('descripcion'),
        FieldPanel('equipo1', classname="field choice_field select"),
        FieldPanel('equipo2', classname="field choice_field select"),
        FieldPanel('fecha'),
        FieldPanel('hora'),
        ImageChooserPanel('photo'),
    ]

    class Meta:
        verbose_name = "Evento"
        verbose_name_plural = "Eventos"

    def __unicode__(self):
        return self.descripcion

# PAGES
class Home(Page):
    uso_noticias_descatadas = models.BooleanField(blank=True, default=False)
    event = models.ForeignKey(
        'core.Event',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    parent_page_types = []
    subpage_types = ['core.NewsCategory', 'core.IndexReporters',
                     'core.StaticPage', 'core.BlogIndexPage']

    content_panels = [
        FieldPanel("title"),
        FieldPanel("uso_noticias_descatadas"),
        InlinePanel('noticias_destacadas', label="Noticias Destacadas", min_num=7, max_num=7),
    ]

    sidebar_content_panels = [
         SnippetChooserPanel('event'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenido'),
    ])

    class Meta:
        verbose_name = "Home"
        verbose_name_plural = "Home"

    def __unicode__(self):
         return '%s' % self.title

    def save(self, *args, **kwargs):
        slug = slugify(self.title)
        self.slug = slug
        super(Home, self).save(*args, **kwargs)

    def article_on_screen(self):
        articles = NewsArticlesOnScreen.objects.live().order_by("-first_published_at").last()
        return articles

    def api_(self):
        lista = []
        request = urllib2.Request("http://futbolview.com/api2/articles/")
        response = urllib2.urlopen(request)
        resp_parsed = json.loads(response.read())
        for i in resp_parsed:
            article = ArticleAPI()
            article.title = i.get('title')
            article.introduction = i.get('introduction')
            article.url_path = i.get('full_url')
            article.photo = i.get('photo').get('photo_rendition')
            lista.append(article)
        return lista

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response

class NewsCategory(Page):

    prioridad = models.IntegerField(default=0)

    parent_page_types = ['core.Home']
    subpage_types = ['core.NewsArticles', 'core.NewsArticlesOnScreen']

    content_panels = [
        FieldPanel("title"),
    ]

    class Meta:
        verbose_name = "Categoria de Noticias"
        verbose_name_plural = "Categoria de Noticias"

    def __str__(self):
         return '%s' % self.title

    def set_url_path(self, parent):
          """
          Override
          """
          if parent:
              if parent.url_path.endswith('//'):
                  parent.url_path = parent.url_path[-1].replace("//", "/")
              else:
                  if self.url_path.endswith('//'):
                      self.url_path = parent.url_path + self.slug
          else:
              # a page without a parent is the tree root, which always has a url_path of '/'
              self.url_path = '/'
          return self.url_path

    # Serve
    def serve(self, request):
        localizacion = int(request.GET.get('localizacion', '0'))
        response = render(request, self.template, {'self': self, 'localizacion': localizacion})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response

class VideoArchive(Page):
    content_panels = [
        FieldPanel("title"),
    ]
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenido'),
    ])
    parent_page_types = []
    subpage_types = ['core.NewsArticlesOnScreen']

    class Meta:
        verbose_name = "videoteca"

    def set_url_path(self, parent):
        """
        Override
        """
        if parent:
            if parent.url_path in "//":
                parent.url_path = parent.url_path.replace("//", "")
            else:
                if parent.url_path.endswith('/'):
                    if self.slug:
                        self.url_path = parent.url_path + self.slug + "/"
        else:
            # a page without a parent is the tree root, which always has a url_path of '/'
            self.url_path = '/'
        return self.url_path

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response


class NewsArticles(Page, ArticleFields):
    contenido = StreamField(BlockStreamFieldContentArticleWeb(), blank=False)
    parent_page_types = ['core.NewsCategory']
    subpage_types = []
    content_panels = [
        FieldPanel("titulo"),
        ImageChooserPanel("photo"),
        FieldPanel('localizacion', classname="field choice_field select"),
        StreamFieldPanel('contenido'),
        FieldPanel('evento'),
    ]
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenido'),
    ])

    class Meta:
        verbose_name = "articulo"

    def __unicode__(self):  # __unicode__ en Python 2
         return '%s' % self.titulo

    def set_url_path(self, parent):
        """
        Override
        """
        if parent:
            if parent.url_path in "//":
                parent.url_path = parent.url_path.replace("//", "")
            else:
                if parent.url_path.endswith('/'):
                    if self.slug:
                        self.url_path = parent.url_path + self.slug + "/"
        else:
            # a page without a parent is the tree root, which always has a url_path of '/'
            self.url_path = '/'
        return self.url_path

    def save(self, *args, **kwargs):
        self.on_screen = False
        if self.categoria is not None:
            parent_page = get_object_or_404(Page, id=self.categoria_id).specific
            # PROCESO PARA MOVER LA PÁGINA
            # 1.- EL PATH
            # 2.- EL SET URL PATH
            # self.move(parent_page, pos='last-child')
        else:
            parent_page = NewsCategory.objects.get(id=self.get_ancestors().type(NewsCategory).last().id)

        self.categoria = parent_page
        self.title = self.titulo
        try:
            self.seo_title = "El Noticiero | "+unicode(self.titulo)
        except:
            pass

        self.photo_fb_share = get_share_image(1, self.photo)
        self.photo_tw_share = get_share_image(2, self.photo)

        if self.contenido is not None:
            self.introduction = format_intro(self.contenido)
            self.search_description = self.introduction

        codigo = get_codeUrl(self.id)
        slug = slugify(self.title+" "+str(codigo))
        self.slug = slug
        self.set_url_path(parent_page)
        super(NewsArticles, self).save(*args, **kwargs)

    # Serve
    def serve(self, request):
        form = EmailForm()
        # url_path = request.build_absolute_uri()
        # if '?' in url_path:
        #    raise Http404
        response = render(request, self.template, {'self': self, "form": form})
        response['Cache-Control'] = 'max-age=300'
        return response

class IndexReporters(Page):

    content_panels = [
        FieldPanel("title"),
    ]
    parent_page_types = ['Home']
    subpage_types = ['Reporter']

    class Meta:
        verbose_name_plural = "Reporteros"

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response

class Reporter(Page):
    name = models.CharField(max_length=220, verbose_name='Nombres')
    photo = models.ForeignKey('wagtailimages.Image', null=True, on_delete=models.SET_NULL, related_name='+',
                              verbose_name="Foto")

    email = models.EmailField(null=True, blank=True, verbose_name='Email')

    fb = models.CharField(null=True, blank=True, max_length=120, verbose_name='Facebook')
    tw = models.CharField(null=True, blank=True, max_length=120, verbose_name='Twitter')
    ins = models.CharField(null=True, blank=True, max_length=120, verbose_name='Instagram')

    parent_page_types = ['IndexReporters']
    subpage_types = []

    content_panels = [
        FieldPanel("name"),
        ImageChooserPanel("photo"),
        MultiFieldPanel(
            [
                FieldPanel('fb'),
                FieldPanel('tw'),
                FieldPanel('ins'),
            ],
            heading="Redes Sociales",
        ),
        FieldPanel('email'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenido'),
    ])

    class Meta:
        verbose_name = "Reportero"
        verbose_name_plural = "Reporteros"

    def set_url_path(self, parent):
        """
        Override
        """
        if parent:
            if parent.url_path in "//":
                parent.url_path = parent.url_path.replace("//", "")
            else:
                if parent.url_path.endswith('/'):
                    if self.slug:
                        self.url_path = parent.url_path + self.slug + "/"
        else:
            # a page without a parent is the tree root, which always has a url_path of '/'
            self.url_path = '/'

        return self.url_path

    def full_clean(self, *args, **kwargs):
        # first call the built-in cleanups (including default slug generation)
        super(Reporter, self).full_clean(*args, **kwargs)
        try:
            self.seo_title = "El Noticiero | "+unicode(self.name)
        except:
            pass
        self.title = self.name
        slug = slugify(self.title)
        self.slug = slug

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response


from django.db.models import Q


# Create De Boca en Boca page temporary
class BlogIndexPage(Page):
    parent_page_types = ['core.Home']
    subpage_types = ['core.BlogNewsPage']
    content_panels = [
        FieldPanel("title", classname='full title'),
    ]

    # Serve
    def serve(self, request):
        # # migrate pages
        # parent_page = Page.objects.live().get(slug='de-boca-en-boca')
        # entretenimiento = Page.objects.live().get(slug='entretenimiento')
        # pages = Page.objects.live().specific(
        # ).filter(Q(newsarticlesonscreen__localizacion=1)).filter(Q(newsarticlesonscreen__categoria=entretenimiento)
        #                                                            ).order_by("-first_published_at")[:30]
        # for p in pages:
        #     page = BlogNewsPage(
        #             search_description='',
        #             seo_title='',
        #             show_in_menus=False,
        #             slug=slugify(p.title),
        #             title=p.title
        #     )
        #     page.contenido = p.contenido
        #     page.video = p.video
        #     parent_page.add_child(instance=page)
        #     page.photo = p.photo
        #     page.photo_fb_share = get_share_image_dbeb(1, page.photo)
        #     page.photo_tw_share = get_share_image_dbeb(2, page.photo)
        #     revision = page.save_revision(user=self.owner)
        #     revision.publish()
        #     page.save()
        response = render(request, self.template,
                          {'self': self, 'page': self,
                           'pages': BlogNewsPage.objects.live().order_by("-first_published_at")[:30]
                           })
        response['Cache-Control'] = 'max-age=120'  # 60
        return response


class NewsArticlesOnScreen(Page, ArticleFields):
    video = models.URLField(verbose_name='Video')
    contenido = StreamField(BlockStreamFieldContentArticleOnScreen(), blank=False)
    reportero = models.ForeignKey(
        'Reporter',
        null=True,
        blank=True,
        related_name='+'
    )
    parent_page_types = ['core.VideoArchive',  'core.NewsCategory', 'core.BlogIndexPage']
    subpage_types = []

    content_panels = [
        FieldPanel('titulo'),
        ImageChooserPanel('photo'),
        FieldPanel('video'),
        FieldPanel('localizacion', classname="field choice_field select"),

        FieldPanel('reportero'),

        FieldPanel('evento'),
        StreamFieldPanel('contenido'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenido'),
    ])

    class Meta:
        verbose_name = "Artículo Pantalla 1"
        verbose_name_plural = "Artículos de Pantalla 1"

    def save(self, *args, **kwargs):
        is_new = self.id is None
        super(NewsArticlesOnScreen, self).save(*args, **kwargs)  # important order these line
        if self.categoria is not None:
            parent_page = get_object_or_404(Page, id=self.categoria.id).specific
        else:
            parent_page = NewsCategory.objects.get(id=self.get_ancestors().type(NewsCategory).last().id)
        self.title = self.titulo
        codigo = get_codeUrl(self.id)
        slug = slugify(self.title+" "+str(codigo))
        self.slug = slug
        self.on_screen = True
        self.categoria = parent_page
        try:
            self.seo_title = "El Noticiero | "+unicode(self.title)
        except:
            pass
        self.set_url_path(parent_page)
        self.photo_fb_share = get_share_image(3, self.photo)
        self.photo_tw_share = get_share_image(4, self.photo)
        if self.contenido is not None:
            self.introduction = format_intro(self.contenido)
            self.search_description = self.introduction

        # build clone
        parent_page = Page.objects.get(slug='de-boca-en-boca')  # Hard Code
        if is_new and self.categoria.slug == 'entretenimiento' and self.localizacion_id == 1:
            try:
                seo_title = "De Boca en Boca | "+unicode(self.title)
            except:
                seo_title = ""
            page = BlogNewsPage(
                    search_description='',
                    seo_title=seo_title,
                    show_in_menus=False,
                    slug=slugify(self.title),
                    title=self.title,
                    owner=self.owner,
                    first_published_at=self.first_published_at
            )
            page.contenido = self.contenido
            page.video = self.video
            page.photo = self.photo
            page.photo_fb_share = get_share_image_dbeb(1, self.photo)
            page.photo_tw_share = get_share_image_dbeb(2, self.photo)
            parent_page.add_child(instance=page)
            revision = page.save_revision(user=self.owner)
            revision.publish()
            page.save()

    # Serve
    def serve(self, request):
        # url_path = request.build_absolute_uri()
        # if '?' in url_path:
        #    raise Http404
        response = render(request, self.template, {'self': self})
        response['Cache-Control'] = 'max-age=300'
        return response


# Static TV en Vivo
class BlockHtmlEmbed(blocks.StreamBlock):
    htmlembed = RawHTMLBlock(icon="icon icon-code", label="Insertar Código",
                             template="core/includes/static-pages/htmlembed.html")


class StaticPage(Page):
    parent_page_types = ['core.Home']
    subpage_types = []

    contenido = StreamField(BlockHtmlEmbed())
    content_panels = [
        FieldPanel("title", classname='full title'),
        StreamFieldPanel('contenido',),
    ]

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self, 'page': self})
        response['Cache-Control'] = 'max-age=120'  # 60
        return response


# Create Articles for 'De Boca en Boca' page
class ArticleFieldsBlog(models.Model):
    photo = models.ForeignKey('wagtailimages.Image', null=True, on_delete=models.SET_NULL, related_name='+',
                              verbose_name="Foto de Artículo")

    photo_fb_share = models.ImageField(upload_to='shares/blognews/fb', null=True, blank=True)
    photo_tw_share = models.ImageField(upload_to='shares/blognews/tw', null=True, blank=True)
    introduction = models.TextField(max_length=200, null=True, blank=True)

    class Meta:
        abstract = True


class BlogNewsPage(Page, ArticleFieldsBlog):
    parent_page_types = ['core.BlogIndexPage']
    subpage_types = []

    video = models.URLField(verbose_name='Video')
    contenido = StreamField([
        ("paragraph", blocks.RichTextBlock(icon="icon icon-pilcrow",
                                           label="Párrafo")),
        ("htmlembed", RawHTMLBlock(icon="icon icon-code",
                                   label="Insertar Código"))
    ])

    content_panels = [
        FieldPanel("title", classname='full title'),
        ImageChooserPanel("photo"),
        FieldPanel("video"),
        StreamFieldPanel("contenido"),
    ]

    # Serve
    def serve(self, request):
        response = render(request, self.template, {'self': self, 'page': self})
        response['Cache-Control'] = 'max-age=300'
        return response