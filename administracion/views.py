#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.db import transaction
from django.db.models import Q
from django.http import JsonResponse

from core.models import *
from .models import *
from administracion.formularios.FormAdmin import *

class PublicationArticles(TemplateView):
    template_name = "administracion/publicaciones.html"

    def get(self, request, *args, **kwargs):
        parametro = False
        flag = False
        test = False
        lista_web = []
        lista_tv = []
        total_publicaciones = 0

        date_t = request.GET.get('fecha', '').split("-")
        buscador = BuscadorReportes()

        if request.GET.get('fecha', '') != '':
            # datetime.datetime.strptime(request.GET.get('fecha', ''), '%Y-%M-%d').date()
            if not parametro:
                test = True
                lista_web = NewsArticles.objects.live().filter(first_published_at__year=date_t[0],
                                                               first_published_at__month=date_t[1],
                                                               first_published_at__day=date_t[2]
                                                               ).order_by("first_published_at")
                lista_tv = NewsArticlesOnScreen.objects.live().filter(first_published_at__year=date_t[0],
                                                                      first_published_at__month=date_t[1],
                                                                      first_published_at__day=date_t[2]
                                                                      ).order_by("first_published_at")
                if len(lista_web) == 0 and len(lista_tv) == 0:
                    flag = True
                total_publicaciones = len(Page.objects.live().filter(depth=5).filter(first_published_at__year=date_t[0],
                                                                      first_published_at__month=date_t[1],
                                                                      first_published_at__day=date_t[2]
                                                                      ).order_by("first_published_at"))
        return render(request, self.template_name,
                      {'articles_web': lista_web, 'resultado': total_publicaciones,
                       'flag': flag, 'parametro': parametro,
                       'buscador': buscador,
                       'validator': test, 'articles_tv': lista_tv})

@csrf_exempt
def eventos_paginas(request):
    '''
    View that control
    :param request:
    :return:
    '''
    query = []
    contador = 0
    indicador = 0
    cont = 1
    selected = 0
    buscador = BuscadorReportes(request.GET)

    if buscador.is_valid():
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
            query = Page.objects.live().filter(depth=5).type((NewsArticles, NewsArticlesOnScreen)).specific(
            ).filter(Q(newsarticles__fecha_pub__range=(fecha_ini, fecha_final)) |
            Q(newsarticlesonscreen__fecha_pub__range=(fecha_ini, fecha_final))).order_by("first_published_at")

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                query = Page.objects.live().filter(depth=5).type((NewsArticles, NewsArticlesOnScreen)).specific(
                ).filter(Q(newsarticles__fecha_pub__range=(fecha_ini, fecha_final)) |
                Q(newsarticlesonscreen__fecha_pub__range=(fecha_ini, fecha_final))).order_by("first_published_at")

                for cab_vta in query:
                    sel_event = request.POST.get("evento-"+str(cont))

                    if sel_event:
                        selected += 1
                        if sel_event == 'on':
                            cab_vta.evento = True
                            cab_vta.save()

                    cont += 1

                if selected == 0:
                    contador += 1
                    messages.error(request, u"Debe Seleccionar al menos un artículo para continuar "
                                            u"con el proceso")
        if contador > 0:
            transaction.rollback()
        else:
            messages.success(request, u"Se realizó exitosamente el procedimiento")

    return render(request, 'administracion/eventos.html', {"buscador": buscador, "objetos": query,
                                                           "indicador": indicador})

def mailing_get(request):
    '''
    View Ajax for use list subscribe
    :param request:
    :return:
    '''
    pass
    # form = EmailForm(request.GET)
    # response_data = {}
    #
    # if form.is_valid():
    #     cd = form.cleaned_data
    #     email = cd['email']
    #     page_id = request.GET.get('page')
    #
    #     # Save Subcriptor
    #     subscriptor = Subscriptor()
    #     subscriptor.email = email
    #     subscriptor.page = get_object_or_404(Page, id=page_id)
    #     subscriptor.save()
    #
    #     response_data['result'] = 'Create subscriptor successful!'
    #     response_data['status'] = '1'
    #     response_data['email'] = subscriptor.email
    #     response = JsonResponse(response_data)
    #     return response
    # else:
    #     response_data['result'] = 'El correo enviado es inválido'
    #     response_data['status'] = '0'
    #     response = JsonResponse(response_data)
    #     return response

