# -*- coding: UTF-8 -*-
from django.conf.urls import url
from administracion.views import PublicationArticles, eventos_paginas, mailing_get

__author__ = 'developerbusensor'

urlpatterns = [
    url(r'^publicaciones/$', PublicationArticles.as_view(), name="publications"),
    url(r'^mailing/get/$', mailing_get, name="mailing_get"),
    # url(r'^eventos/$', eventos_paginas, name="eventos_paginas"),
]