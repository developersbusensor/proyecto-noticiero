#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import forms
import datetime

class BuscadorReportes(forms.Form):
    fecha_ini = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BuscadorReportes, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['placeholder'] = "año-mes-día"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "año-mes-día"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"

class EmailForm(forms.Form):

    email = forms.EmailField()


    def getEmail(self):
        print "self: ", self
        print "ejejejje: ", self.cleaned_data['email']
        return self.data['email']

    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = "Escribe tu correo"

