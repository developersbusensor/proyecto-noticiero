# coding=utf-8
from django.conf.urls import url
from search.views import search

__author__ = 'developerbusensor'

urlpatterns = [
    url(r'^search/$', search, name="search"),
]