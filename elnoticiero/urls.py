from django.conf.urls import include, patterns, url
from django.conf import settings
from django.contrib import admin

from django.views.generic import RedirectView
from core.views import AjaxLoadMoreArticles, SubscribeViews
from administracion import urls_administracion

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls

from wagtail.utils.urlpatterns import decorate_urlpatterns

# import debug_toolbar

urlpatterns = [
    url(r'^django-admin/', include(admin.site.urls)),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),

    url(r'^search/$', 'search.views.search', name='search'),
    url(r'^', include(urls_administracion, namespace='administracion')),

    # AJAX
    url(r'^load/ajax/$', AjaxLoadMoreArticles.as_view(), name="load_more"),
    url(r'^suscribete/$', SubscribeViews.as_view(), name="suscribe"),

    # Iframes
    url(r'^iframe/nacionales/$', 'core.views.iframe_nacional', name="iframe_nacional"),
    url(r'^iframe/internacionales/$', 'core.views.iframe_internacional', name="iframe_internacional"),
    url(r'^iframe/entretenimiento/$', 'core.views.iframe_entretenimiento', name="iframe_entretenimiento"),

    # url(r'^__debug__/', include(debug_toolbar.urls)),

    # Wagtail
    url(r'', include(wagtail_urls)),
]

handler404 = 'core.views.miError404'
handler403 = 'core.views.miError403'
handler500 = 'core.views.miError500'

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)