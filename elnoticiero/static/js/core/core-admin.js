/* Created by developerbusensor on 20/1/16. */

var halloPlugins = {
    halloformat: {},
    halloheadings: {formatBlocks: ['p', 'h2', 'h3', 'h4', 'h5']},
    hallolists: {},
    hallohr: {},
    halloreundo: {},
    hallowagtaillink: {},
    hallorequireparagraphs: {},
    hallocleanhtml: {
        allowedTags: ['p', 'ol', 'ul', 'li']
    }
};

