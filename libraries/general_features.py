#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys
from bs4 import BeautifulSoup
from PIL import Image
from django.template.defaultfilters import slugify


__author__ = 'Roberto Noboa'

def get_codeUrl(id):
    '''
    Function get code url concat id with string 0000000
    :param id:
    :return:
    '''
    codigo = str(id).zfill(7)
    return codigo

def element_list(list, count):
    '''
    Function element_list
    :param list:
    :param count:
    :return:
    '''
    try:
        for (i, item1) in enumerate(list):
            return list[i+count]
    except:
        return list

def format_intro(value):
    '''
    Function format_intro get first paragraph and segment
    the main part
    :param value:
    :return:
    '''
    try:
        soup = BeautifulSoup(str(value), 'html.parser')
        intro = soup.p.get_text()
    except:
        intro = ""
    return intro


def get_share_image(mode, value):
    '''
        This Function build the image share about mode
        Example:
        Mode -> Facebook Web (1)
        Mode -> Twitter  Web (2)
        Mode -> Facebook TV (3)
        Mode -> Twitter  TV (4)
        :return:
    '''
    if mode == 1:  # Facebook Web
        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1200x630').file))   # background
            path_photo_fb = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Facebook-image.png")  # F
            path_img = "media/shares/fb/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630').id
                                                                      ).zfill(7)+".jpg"

            if value.width < 1200 or value.height < 630:
                img = path_photo_article.resize((1200, 630))
                img.paste(path_photo_fb, (0, 0), path_photo_fb)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_fb, (0, 0), path_photo_fb)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass
            # Error en Facebook

    elif mode == 2:  # Twitter Web
        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1024x512').file))   # background
            path_photo_tw = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Twitter-image_fix.png")
            path_img = "media/shares/tw/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630'
                                                                                          ).id).zfill(7)+".jpg"

            if value.width < 1024 or value.height < 512:
                img = path_photo_article.resize((1024, 512))
                img.paste(path_photo_tw, (0, 0), path_photo_tw)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_tw, (0, 0), path_photo_tw)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass
            # Error en Twitter Article

    elif mode == 3:  # Facebook TV
        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1200x630').file))   # background
            path_photo_fb = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Facebook_tv.png")  # F
            path_img = "media/shares/fb/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630'
                                                                                          ).id).zfill(7)+".jpg"

            if value.width < 1200 or value.height < 630:
                img = path_photo_article.resize((1200, 630))
                img.paste(path_photo_fb, (0, 0), path_photo_fb)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_fb, (0, 0), path_photo_fb)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass

    elif mode == 4:  # Twitter TV
        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1024x512').file))   # background
            path_photo_tw = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Twitter-tv_fix.png")  # F
            path_img = "media/shares/tw/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630'
                                                                                          ).id).zfill(7)+".jpg"

            if value.width < 1024 or value.height < 512:
                img = path_photo_article.resize((1024, 512))
                img.paste(path_photo_tw, (0, 0), path_photo_tw)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_tw, (0, 0), path_photo_tw)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass


def get_share_image_dbeb(mode, value):
    '''
        This Function build the image share about mode
        Example:
        Mode -> Facebook Web (1)
        Mode -> Twitter  Web (2)
        :return:
    '''
    if mode == 1:  # Facebook Web
        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1200x630').file))   # background
            path_photo_fb = Image.open("elnoticiero/static/images/shares-dbeb/dbeb-share-fb.png")  # F
            path_img = "media/shares/dbeb/fb/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630').id
                                                                      ).zfill(7)+".jpg"

            if value.width < 1200 or value.height < 630:
                img = path_photo_article.resize((1200, 630))
                img.paste(path_photo_fb, (0, 0), path_photo_fb)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_fb, (0, 0), path_photo_fb)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass
            # Error en Facebook

    else:  # Twitter Web

        try:
            path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1024x512').file))   # background
            path_photo_tw = Image.open("elnoticiero/static/images/shares-dbeb/dbeb-share-tw.png")
            path_img = "media/shares/dbeb/tw/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630'
                                                                                          ).id).zfill(7)+".jpg"

            if value.width < 1024 or value.height < 512:
                img = path_photo_article.resize((1024, 512))
                img.paste(path_photo_tw, (0, 0), path_photo_tw)
                img.save(path_img, optimize=True, quality=77)
                return path_img
            else:
                path_photo_article.paste(path_photo_tw, (0, 0), path_photo_tw)
                path_photo_article.save(path_img, optimize=True, quality=77)
                return path_img
        except:
            pass
            # Error en Twitter Article
