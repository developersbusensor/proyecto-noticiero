# -*- coding: utf8 -*-
from urllib2 import urlopen
from pyjpegoptim import JpegOptim

from PIL import Image, ExifTags

import urllib, cStringIO
from PIL import ImageDraw, ImageFont
# from willow.image import Image as WillowImage

URL = 'http://www.tctelevision.com/elarboldelosdeseos/user_images/77537.jpg'
path_photo_fb = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Facebook-image.png")
URL_END = '/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/77537.jpg'
API_URL = cStringIO.StringIO(urllib.urlopen(URL).read())

# img = Image.open(cStringIO.StringIO(urllib.urlopen(URL).read()))
# img.paste(path_photo_fb, (0, 0), path_photo_fb)
# draw = ImageDraw.Draw(img)
# draw.text((0, 0), "Roberto Noboa ", (255, 255, 255))
# img.save(URL_END, optimize=True, quality=77)

def resize_and_crop(img_path, modified_path, size, crop_type='top'):
    """
    Resize and crop an image to fit the specified size.
    args:
        img_path: path for the image to resize.
        modified_path: path to store the modified image.
        size: `(width, height)` tuple.
        crop_type: can be 'top', 'middle' or 'bottom', depending on this
            value, the image will cropped getting the 'top/left', 'midle' or
            'bottom/rigth' of the image to fit the size.
    raises:
        Exception: if can not open the file in img_path of there is problems
            to save the image.
        ValueError: if an invalid `crop_type` is provided.
    """
    # If height is higher we resize vertically, if not we resize horizontally
    img = Image.open(img_path)

    # Get current and desired ratio for the images
    img_ratio = img.size[0] / float(img.size[1])
    ratio = size[0] / float(size[1])
    # The image is scaled/cropped vertically or horizontally depending on the ratio
    if ratio > img_ratio:
        img = img.resize((size[0], size[0] * img.size[1] / img.size[0]),
                Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, img.size[0], size[1])
        elif crop_type == 'middle':
            box = (0, (img.size[1] - size[1]) / 2, img.size[0], (img.size[1] + size[1]) / 2)
        elif crop_type == 'bottom':
            box = (0, img.size[1] - size[1], img.size[0], img.size[1])
        else:
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    elif ratio < img_ratio:
        img = img.resize((size[1] * img.size[0] / img.size[1], size[1]),
                Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, size[0], img.size[1])
        elif crop_type == 'middle':
            box = ((img.size[0] - size[0]) / 2, 0, (img.size[0] + size[0]) / 2, img.size[1])
        elif crop_type == 'bottom':
            box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    else:
        img = img.resize((size[0], size[1]),
                Image.ANTIALIAS)
        # If the scale is the same, we do not need to crop

    img.save(modified_path)


try:
    image = Image.open(API_URL)
    for orientation in ExifTags.TAGS.keys():
        if ExifTags.TAGS[orientation] == 'Orientation':
            break

    exif = dict(image._getexif().items())

    if exif[orientation] == 3:
        image = image.rotate(180, expand=False)
    elif exif[orientation] == 6:
        image = image.rotate(270, expand=False)
    elif exif[orientation] == 8:
        image = image.rotate(90, expand=False)

    print(image.size, image.mode)

    new_img = image.resize((1200, 630))
    new_img.paste(path_photo_fb, (0, 0), path_photo_fb)
    new_img.save(URL_END)
    new_img.close()

except (AttributeError, KeyError, IndexError):
    # cases: image don't have getexif
    pass


# try:
#     path_photo_article = Image.open("media/"+str(value.get_rendition('fill-1200x630').file))   # background
#     path_photo_fb = Image.open("elnoticiero/static/images/share_social/Sobreimpresion_Facebook-image.png")
#     path_img = "media/shares/fb/"+slugify(str(value))+"-"+str(value.get_rendition('fill-1200x630').id).zfill(7)+".jpg"
#
#     if value.width < 1200 or value.height < 630:
#         img = path_photo_article.resize((1200, 630))
#         img.paste(path_photo_fb, (0, 0), path_photo_fb)
#         img.save(path_img, optimize=True, quality=77)
#         return path_img
#     else:
#         path_photo_article.paste(path_photo_fb, (0, 0), path_photo_fb)
#         path_photo_article.save(path_img, optimize=True, quality=77)
#         return path_img
# except:
#     pass
#     # Error en Facebook

# img = Image.open(resultado)
# img.convert('L')
# img.show()
# img.save('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/77537.jpg')

# t = JpegOptim('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/elnoticiero/static/', quality=80)
# t.save('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/')
# t.close()

# im = Image.open('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/media/_Thor.png')
# im.save('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/est.jpg', optimize=True)


'''
img = Image.open('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/media/.png')

# imgtest = Image.open('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/.jpg')
if img.mode == 'RGBA' or img.mode == 'LA' or img.mode == 'RGBX' or img.mode == 'RGBa':
    img.load()  # needed for split()
    background = Image.new('RGB', img.size, (255, 255, 255))  # color white
    background.paste(img, mask=img.split()[3])  # 3 is the alpha channel
    background.save('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/tes.jpg', 'JPEG', optimize=True)

elif img.mode == 'RGB':
    pass

else:
    # Convertir a RGB
    rgbimg = img.convert('RGB')  # convierte a RGB
    rgbimg.save('/Users/developerbusensor/Documents/SitioNoticiasTC/proyecto-noticiero/te.jpg', 'JPEG', optimize=True)
'''


